package org.dhc.dhclite;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
	public void split () throws java.lang.Exception
	{
		String regex = "([^\"]\\S*|\".+?\")\\s*";
		String string = "ADD r2, r3 \"ADD r2, r3\"    \"test one\"";
 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(string);
 
		while (matcher.find()) {
		    System.out.println(matcher.group(1).replace("\"", ""));
		}
	}
}
