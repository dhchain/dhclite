package org.dhc.twitter;

import java.io.Serializable;

public class TwitterAccount implements Serializable {

	private static final long serialVersionUID = 2426679355046726832L;
	
	private String name;
	private String dhcAddress;
	private long timeStamp;
	private String description;
	private String transactionId;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int hashCode() {
		return dhcAddress.hashCode();
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof TwitterAccount)) {
			return false;
		}
		TwitterAccount other = (TwitterAccount) obj;
		return dhcAddress.equals(other.dhcAddress);
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getDhcAddress() {
		return dhcAddress;
	}
	public void setDhcAddress(String dhcAddress) {
		this.dhcAddress = dhcAddress;
	}

}
