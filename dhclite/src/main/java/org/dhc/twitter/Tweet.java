package org.dhc.twitter;

import java.io.Serializable;
import java.security.GeneralSecurityException;

import org.dhc.blockchain.Keywords;
import org.dhc.util.CryptoUtil;
import org.dhc.util.DhcLogger;

public class Tweet implements Serializable {

	private static final long serialVersionUID = -8541311134915485210L;
	private static final DhcLogger logger = DhcLogger.getLogger();
	
	private String transactionId;
	private String sender;
	private String recipient;
	private long timeStamp;
	private String text;
	private Keywords keywords;
	
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public long getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getSenderAddress() {
		try {
			return CryptoUtil.getDhcAddressFromKey(CryptoUtil.loadPublicKey(sender)).toString();
		} catch (GeneralSecurityException e) {
			logger.debug(e.getMessage(), e);
		}
		return null;
	}
	
	public Keywords getKeywords() {
		return keywords;
	}
	public void setKeywords(Keywords keywords) {
		this.keywords = keywords;
	}
	
	public String getFromTwitterAccount() {
		if(keywords == null) {
			return getSenderAddress();
		}
		String from = keywords.get("from");
		if(from == null) {
			return getSenderAddress();
		}
		return from;
	}


}
