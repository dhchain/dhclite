package org.dhc.lite;

import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dhc.blockchain.Keywords;
import org.dhc.blockchain.SendTransactionMessage;
import org.dhc.blockchain.Transaction;
import org.dhc.blockchain.TransactionData;
import org.dhc.blockchain.TransactionOutput;
import org.dhc.gui.promote.JoinInfo;
import org.dhc.gui.promote.JoinLine;
import org.dhc.gui.transaction.TransactionTimestampComparator;
import org.dhc.lite.post.GetKeywordsThinRequest;
import org.dhc.lite.post.GetKeywordsThinResponse;
import org.dhc.lite.post.Ratee;
import org.dhc.lite.post.Rating;
import org.dhc.lite.post.SearchPostsThinRequest;
import org.dhc.lite.post.SearchPostsThinResponse;
import org.dhc.lite.post.SearchRateeThinRequest;
import org.dhc.lite.post.SearchRateeThinResponse;
import org.dhc.lite.post.SearchRateesThinRequest;
import org.dhc.lite.post.SearchRateesThinResponse;
import org.dhc.lite.post.SearchRatingsForRaterThinRequest;
import org.dhc.lite.post.SearchRatingsForRaterThinResponse;
import org.dhc.lite.post.SearchRatingsThinRequest;
import org.dhc.lite.post.SearchRatingsThinResponse;
import org.dhc.network.Network;
import org.dhc.network.Peer;
import org.dhc.network.PeerSync;
import org.dhc.util.Applications;
import org.dhc.util.Base58;
import org.dhc.util.Coin;
import org.dhc.util.Constants;
import org.dhc.util.CryptoUtil;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.Encryptor;

public class DhcLiteHelper {
	
	private static final DhcLogger logger = DhcLogger.getLogger();
	
	public static Coin getBalance(DhcAddress address) {
		checkNetwork();
		Coin balance = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			GetBalanceReply reply = (GetBalanceReply) peer.sendSync(new GetBalanceRequest(address), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			balance  = reply.getBalance();
			if(balance == null) {
				continue;
			}
			break;
		}
		if(balance == null) {
			logger.info("Could not get balance");
			return balance;
		}
		return balance;
	}
	
	public static JoinInfo getJoinInfo(DhcAddress address, Coin amount) {
		checkNetwork();
		JoinInfo result = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			GetJoinInfoThinReply reply = (GetJoinInfoThinReply) peer.sendSync(new GetJoinInfoThinRequest(DhcAddress.getMyDhcAddress(), address, amount), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			result  = reply.getJoinInfo();

			if(result == null) {
				continue;
			}
			break;
		}
		logger.info("getJoinInfo for address={}, amount={} returned {} ", address, amount, result);
		if(result != null && !result.isValid()) {
			result = null;
		}
		return result;
	}
	
	public static Set<JoinLine> getJoinLines() {
		checkNetwork();
		Set<JoinLine> set = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			GetJoinLinesReply reply = (GetJoinLinesReply) peer.sendSync(new GetJoinLinesRequest(DhcAddress.getMyDhcAddress()), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			set  = reply.getJoinLines();
			if(set == null) {
				continue;
			}
			break;
		}
		return set;
	}
	
	public static Set<Transaction> getMempoolTransactions() {
		checkNetwork();
		Set<Transaction> set = new HashSet<>();
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			GetMempoolTransactionsReply reply = (GetMempoolTransactionsReply) peer.sendSync(new GetMempoolTransactionsRequest(DhcAddress.getMyDhcAddress()), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			set  = reply.getTransactions();
			if(set == null) {
				continue;
			}
			break;
		}
		if(set != null && !set.isEmpty()) {
			List<Transaction> list = new ArrayList<Transaction>(set);
			Collections.sort(list, new TransactionTimestampComparator());
			set = new LinkedHashSet<Transaction>(list);
		}
		return set;
	}
	
	public static Set<TransactionOutput> getTransactionOutputs() {
		checkNetwork();
		Set<TransactionOutput> set = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			GetTransactionOutputsReply reply = (GetTransactionOutputsReply) peer.sendSync(new GetTransactionOutputsRequest(DhcAddress.getMyDhcAddress()), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			set  = reply.getTransactionOutputs();
			if(set == null) {
				continue;
			}
			break;
		}
		return set;
	}
	
	public static Set<Transaction> getTransactions() {
		checkNetwork();
		Set<Transaction> set = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			GetTransactionsReply reply = (GetTransactionsReply) peer.sendSync(new GetTransactionsRequest(DhcAddress.getMyDhcAddress()), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			set  = reply.getTransactions();
			if(set == null) {
				continue;
			}
			break;
		}
		return set;
	}
	
	public static Set<Transaction> getTransactionsForApp() {
		checkNetwork();
		Set<Transaction> set = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			GetTransactionsForAppReply reply = (GetTransactionsForAppReply) peer.sendSync(new GetTransactionsForAppRequest(Applications.JOIN, DhcAddress.getMyDhcAddress()), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			set  = reply.getTransactions();
			if(set == null) {
				continue;
			}
			break;
		}
		return set;
	}
	
    public static void checkNetwork() {
        List<Peer> peers = Network.getInstance().getMyBucketPeers();
        if(peers.isEmpty()) {
            logger.info("myBucketPeers is empty. Calling PeerSync.executeNow()");
            PeerSync.getInstance().executeAndWait();
        }
    }
    
    public static List<SecureMessage> getSecureMessages(DhcAddress address) {
    	checkNetwork();
		List<SecureMessage> list = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			GetMessagesReply reply = (GetMessagesReply) peer.sendSync(new GetMessagesRequest(address), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			list  = reply.getMessages();
			if(list == null) {
				continue;
			}
			break;
		}
		return list;
    }
    
    public static PublicKey getPublicKey(String address) {
    	checkNetwork();
    	PublicKey result = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			GetSenderPublicKeyReply reply = (GetSenderPublicKeyReply) peer.sendSync(new GetSenderPublicKeyRequest(address), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			result  = reply.getPublicKey();
			if(result == null) {
				continue;
			}
			break;
		}
		return result;
    }
    
    public static Ratee getRatee(DhcAddress dhcAddress, String transactionId) {
    	checkNetwork();
    	Ratee result = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			SearchRateeThinResponse reply = (SearchRateeThinResponse) peer.sendSync(new SearchRateeThinRequest(dhcAddress, transactionId), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			result  = reply.getRatee();
			if(result == null) {
				continue;
			}
			break;
		}
		return result;
    }
    
    public static List<Ratee> getPosts(DhcAddress dhcAddress) {
    	checkNetwork();
    	List<Ratee> result = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			SearchPostsThinResponse reply = (SearchPostsThinResponse) peer.sendSync(new SearchPostsThinRequest(dhcAddress), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			result  = reply.getRatees();
			if(result == null) {
				continue;
			}
			break;
		}
		return result;
    }
    
    public static List<Ratee> searchRatees(String post, Set<String> words) {
    	checkNetwork();
    	List<Ratee> result = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			SearchRateesThinResponse reply = (SearchRateesThinResponse) peer.sendSync(new SearchRateesThinRequest(post, words), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			result  = reply.getRatees();
			if(result == null) {
				continue;
			}
			break;
		}
		return result;
    }
    
    public static List<Rating> searchRatingsForRater(String rater) {
    	checkNetwork();
    	List<Rating> result = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			SearchRatingsForRaterThinResponse reply = (SearchRatingsForRaterThinResponse) peer.sendSync(new SearchRatingsForRaterThinRequest(rater), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			result  = reply.getRatings();
			if(result == null) {
				continue;
			}
			break;
		}
		return result;
    }
    
    public static List<Rating> searchRatings(String rateeName, String transactionId) {
    	checkNetwork();
    	List<Rating> result = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			SearchRatingsThinResponse reply = (SearchRatingsThinResponse) peer.sendSync(new SearchRatingsThinRequest(rateeName, transactionId), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			result  = reply.getRatings();
			if(result == null) {
				continue;
			}
			break;
		}
		return result;
    }
    
    public static Keywords getKeywords(String rateeName, String transactionId) {
    	checkNetwork();
    	Keywords result = null;
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			GetKeywordsThinResponse reply = (GetKeywordsThinResponse) peer.sendSync(new GetKeywordsThinRequest(rateeName, transactionId), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			result  = reply.getKeywords();
			if(result == null) {
				continue;
			}
			break;
		}
		return result;
    }
    
    private static TransactionOutput removeOutput(Coin value, Set<TransactionOutput> outputs) {
        logger.trace("Will look for output with value {}", value);
        TransactionOutput result = null;
        for(TransactionOutput output: outputs) {
            logger.trace("output {}", output);
            if(output.getValue().equals(value)) {
                result = output;
            }
        }
        outputs.remove(result);
        logger.trace("result {}", result);
        return result;
    }
    
    public static String sendDeleteRateeTransaction(String rateeName, String transactionId) {
        Keywords accountKeywords = DhcLiteHelper.getKeywords(rateeName, transactionId);

        if(accountKeywords == null || accountKeywords.isEmpty()) {
            return "Could not retrieve any keywords for " + rateeName;
        }

        Coin amount = Coin.SATOSHI.multiply(2);
        List<Coin> totals = new ArrayList<>();
        totals.add(amount);
        for(String word: accountKeywords.keySet()) {
            if(word.equals(accountKeywords.get(word))) {
                totals.add(amount);
            }
        }
        Set<TransactionOutput> originalOutputs = DhcLiteHelper.getTransactionOutputs();
        Coin sum = Coin.ZERO;
        for(TransactionOutput output: originalOutputs) {
            sum = sum.add(output.getValue());
        }

        if(sum.less(amount.multiply(totals.size()))) {
            return "No inputs available for dhc address " + DhcAddress.getMyDhcAddress() + ". Please check your balance";
        }

        Transaction transaction = new Transaction();
        transaction.createSplitOutputsTransaction(DhcAddress.getMyDhcAddress(), Coin.ZERO, originalOutputs, totals.toArray(new Coin[0]));
        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
        logger.info("sent transaction {}", transaction);

        Set<TransactionOutput> outputs = transaction.getOutputsWithoutChange();

        Keywords keywords = new Keywords();
        keywords.put("delete", transactionId);

        TransactionOutput output = removeOutput(amount, outputs);

        transaction = new Transaction();
        transaction.create(CryptoUtil.getDhcAddressFromString(rateeName), Coin.SATOSHI, Coin.SATOSHI, null, keywords, output, Applications.RATING);
        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
        logger.info("sent transaction {}", transaction);

        for(String word: accountKeywords.keySet()) {
            if(word.equals(accountKeywords.get(word))) {
                Keywords words = keywords.copy();

                DhcAddress recipient = CryptoUtil.getDhcAddressFromString(word);
                output = removeOutput(amount, outputs);

                Transaction keyWordTransaction = new Transaction();
                keyWordTransaction.create(recipient, Coin.SATOSHI, Coin.SATOSHI, null, words, output, Applications.RATING);
                Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(keyWordTransaction));
                logger.info("sent transaction {}", keyWordTransaction);
            }
        }

       return "Sent delete for " + rateeName;
    }
    
    public static Transaction sendCreateRateeTransaction(String post, Set<String> words, String description) {
        DhcAddress ratee = CryptoUtil.getDhcAddressFromString(post);
        Coin amount = Coin.SATOSHI.multiply(2); // one satoshi for amount, another satoshi for fee
        List<Coin> totals = new ArrayList<>();
        totals.add(amount);
        for(@SuppressWarnings("unused") String word: words) {
            totals.add(amount);
        }
        Set<TransactionOutput> originalOutputs = DhcLiteHelper.getTransactionOutputs();
        Coin sum = Coin.ZERO;
        for(TransactionOutput output: originalOutputs) {
            logger.info("output {}", output);
            sum = sum.add(output.getValue());
        }

        logger.info("sum={}, amount.multiply(totals.size())={}", sum, amount.multiply(totals.size()));

        if(sum.less(amount.multiply(totals.size()))) {
        	logger.info("No inputs available for dhc address {}. Please check your balance", DhcAddress.getMyDhcAddress());
            return null;
        }

        Transaction transaction = new Transaction();
        transaction.createSplitOutputsTransaction(DhcAddress.getMyDhcAddress(), Coin.ZERO, originalOutputs, totals.toArray(new Coin[0]));
        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
        logger.info("sent transaction {}", transaction);

        Set<TransactionOutput> outputs = transaction.getOutputsWithoutChange();

        Keywords keywords = new Keywords();
        keywords.put("create", post);
        keywords.put("first", post);
        for(String word: words) {
            keywords.put(word, word);
        }

        TransactionOutput output = removeOutput(amount, outputs);
        TransactionData data = new TransactionData(description);
        transaction = new Transaction();
        transaction.create(ratee, Coin.SATOSHI, Coin.SATOSHI, data, keywords, output, Applications.RATING);
        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
        logger.info("sent transaction {}", transaction);

        for(String word: words) {
            Keywords keywordsLoop = keywords.copy();
            keywordsLoop.put("transactionId", transaction.getTransactionId());
            keywordsLoop.put("first", word);
            DhcAddress recipient = CryptoUtil.getDhcAddressFromString(word);
            output = removeOutput(amount, outputs);

            Transaction keyWordTransaction = new Transaction();
            data = new TransactionData(description);
            keyWordTransaction.create(recipient, Coin.SATOSHI, Coin.SATOSHI, data, keywordsLoop, output, Applications.RATING);
            Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(keyWordTransaction));
            logger.info("sent transaction {}", keyWordTransaction);
        }

        return transaction;
    }
    
    public static String addRating(String name, String transactionId, String comment, String rating) {
        Keywords accountKeywords = DhcLiteHelper.getKeywords(name, transactionId);

        if(accountKeywords == null || accountKeywords.isEmpty()) {
        	return "Could not retrieve any keywords for " + name;
        }

        Coin amount = Coin.SATOSHI.multiply(2);
        List<Coin> totals = new ArrayList<>();
        totals.add(amount);
        for(String word: accountKeywords.keySet()) {
            if(word.equals(accountKeywords.get(word))) {
                totals.add(amount);
            }
        }
        Set<TransactionOutput> originalOutputs = getTransactionOutputs();
        Coin sum = Coin.ZERO;
        for(TransactionOutput output: originalOutputs) {
            sum = sum.add(output.getValue());
        }

        if(sum.less(amount.multiply(totals.size()))) {
            return "no inputs available for dhc address " + DhcAddress.getMyDhcAddress() + ". Please check your balance";
        }

        Transaction transaction = new Transaction();
        transaction.createSplitOutputsTransaction(DhcAddress.getMyDhcAddress(), Coin.ZERO, originalOutputs, totals.toArray(new Coin[0]));
        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
        logger.info("sent transaction {}", transaction);

        Set<TransactionOutput> outputs = transaction.getOutputsWithoutChange();

        Keywords keywords = new Keywords();
        keywords.put("rater", DhcAddress.getMyDhcAddress().toString());
        keywords.put("ratee", name);
        keywords.put("transactionId", transactionId);
        keywords.put("rating", rating);

        TransactionOutput output = removeOutput(amount, outputs);
        TransactionData data = new TransactionData(comment);
        transaction = new Transaction();
        transaction.create(CryptoUtil.getDhcAddressFromString(name), Coin.SATOSHI, Coin.SATOSHI, data, keywords, output, Applications.RATING);
        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
        logger.info("sent transaction {}", transaction);

        for(String word: accountKeywords.keySet()) {
            if(word.equals(accountKeywords.get(word))) {
                Keywords words = keywords.copy();
                words.remove("rater");

                DhcAddress recipient = CryptoUtil.getDhcAddressFromString(word);
                output = removeOutput(amount, outputs);

                Transaction keyWordTransaction = new Transaction();
                data = new TransactionData(comment);
                keyWordTransaction.create(recipient, Coin.SATOSHI, Coin.SATOSHI, data, words, output, Applications.RATING);
                Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(keyWordTransaction));
                logger.info("sent transaction {}", keyWordTransaction);
            }
        }

        logger.debug( "submitted_rating_for {}", name );
        return null;
    }
    
    public static String sendSecureMessage(Coin fee, String recipientDhcAddress, String subject, long expire, String messageBody) {
        logger.info("processAsync() start");
        Network network = Network.getInstance();
        final Coin total = Coin.SATOSHI.add(fee);
        String result;

        Coin balance = getBalance(DhcAddress.getMyDhcAddress());
        if(balance.less(total)) {
            result = "Your balance less than required";
            logger.info(result);
            return result;
        }

        PublicKey recipient = getPublicKey(recipientDhcAddress);

        if(recipient == null) {
            logger.debug("Recipient public key is not found for DHC address {}", recipientDhcAddress);
            result = "Recipient public key is not found for DHC address " + recipientDhcAddress;
            return result;
        }

        TransactionData transactionData = null;
        if(subject != null) {
            try {
                String str = subject + "\n" + messageBody;
                Encryptor encryptor = new Encryptor();
                byte[] encrypted = encryptor.encryptAsymm(str.getBytes(StandardCharsets.UTF_8), recipient);
                str = Base58.encode(encrypted);
                transactionData = new TransactionData(str, expire);
                if(!str.equals(transactionData.getData())) {
                    logger.debug("Encrypted string length is {}", str.length());
                    result = "message is too long";
                    return result;
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                result  = e.getMessage();
                return result;
            }
        }

        Transaction transaction = new Transaction();
        transaction.setApp(Applications.MESSAGING);
        Set<TransactionOutput> outputs = getTransactionOutputs();
        try {
            transaction.create(CryptoUtil.getDhcAddressFromKey(recipient), Coin.SATOSHI, fee, transactionData, null, outputs);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            result = e.getMessage();
            return result;
        }

        network.sendToAllMyPeers(new SendTransactionMessage(transaction));

        logger.info("sent {}", transaction);

        result = "Successfully sent_message \"" + subject + "\" to " + recipientDhcAddress;
        return result;
    }
    
    public static Set<String> getKeywords(String keywords) {
		Set<String> set = new HashSet<String>();
		List<String> strings = split(keywords);
		int counter = 0;
		for(String str: strings) {
			if(counter++ > Keywords.MAX_NUMBER_OF_KEYWORDS) {
				break;
			}
			if(str.length() < 3) {
				continue;
			}
			set.add(str.toLowerCase());
		}
		return set;
	}
    
    public static List<String> split (String string) {
    	List<String> result = new ArrayList<>();
		String regex = "([^\"]\\S*|\".+?\")\\s*";
 
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(string);
 
		while (matcher.find()) {
		    result.add(matcher.group(1).replace("\"", ""));
		}
		return result;
	}

	public static Set<Transaction> findFaucetTransactions(DhcAddress address, String ip) {
		checkNetwork();
		Set<Transaction> result = new HashSet<>();
		List<Peer> peers = Network.getInstance().getMyBucketPeers();
		for (Peer peer : peers) {
			FindFaucetTransactionsReply reply = (FindFaucetTransactionsReply) peer.sendSync(new FindFaucetTransactionsRequest(address, ip), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			Set<Transaction> set  = reply.getTransactions();
			if(set == null) {
				continue;
			}
			result.addAll(set);
			break;
		}
		for (Peer peer : peers) {
			FindFaucetTransactionsReply reply = (FindFaucetTransactionsReply) peer.sendSync(new FindFaucetTransactionsRequest(CryptoUtil.getDhcAddressFromString(ip), ip), Constants.SECOND * 20);
			if(reply == null) {
				continue;
			}
			Set<Transaction> set  = reply.getTransactions();
			if(set == null) {
				continue;
			}
			result.addAll(set);
			break;
		}
		return result;
	}
	
    public static String sendFaucetTransaction(DhcAddress recipient, String ip) {

		try {
			Coin balance = DhcLiteHelper.getBalance(DhcAddress.getMyDhcAddress());
			if(balance.less(Coin.ONE.add(Coin.SATOSHI.multiply(3)))) {
				return "The balance is is not sufficient to create this transaction";
			}
			List<Coin> totals = new ArrayList<>();
			totals.add(Coin.ONE.add(Coin.SATOSHI));
			totals.add(Coin.SATOSHI.add(Coin.SATOSHI));

			Set<TransactionOutput> originalOutputs = DhcLiteHelper.getTransactionOutputs();
			Transaction transaction = new Transaction();
	        transaction.createSplitOutputsTransaction(DhcAddress.getMyDhcAddress(), Coin.ZERO, originalOutputs, totals.toArray(new Coin[0]));
	        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
	        logger.info("sent transaction {}", transaction);
	        Set<TransactionOutput> outputs = transaction.getOutputsWithoutChange();
	        
	        Keywords keywords = new Keywords();
	        keywords.put("ip", ip);
	        TransactionOutput output = removeOutput(Coin.ONE.add(Coin.SATOSHI), outputs);
	        transaction = new Transaction();
	        transaction.create(recipient, Coin.ONE, Coin.SATOSHI, null, keywords, output, Applications.FAUCET);
	        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
	        logger.info("sent transaction {}", transaction);
			
	        keywords = new Keywords(); // cannot reuse previous keywords since it will get different transactionId, blockhash asynchronously from before
	        keywords.put("ip", ip);
	        output = removeOutput(Coin.SATOSHI.add(Coin.SATOSHI), outputs);
	        transaction = new Transaction();
	        transaction.create(CryptoUtil.getDhcAddressFromString(ip), Coin.SATOSHI, Coin.SATOSHI, null, keywords, output, Applications.FAUCET);
	        Network.getInstance().sendToAllMyPeers(new SendTransactionMessage(transaction));
	        logger.info("sent transaction {}", transaction);

		} catch (Exception e) {
			return e.getMessage();
		}
		
		return "Transaction was sent";
	}


}
