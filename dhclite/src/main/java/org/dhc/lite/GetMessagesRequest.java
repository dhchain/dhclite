package org.dhc.lite;

import org.dhc.network.Peer;
import org.dhc.util.DhcAddress;
import org.dhc.util.Message;

public class GetMessagesRequest extends Message {

	private DhcAddress dhcAddress;

	public GetMessagesRequest(DhcAddress dhcAddress) {
		this.dhcAddress = dhcAddress;
	}

	@Override
	public void process(Peer peer) {
		

	}

	public DhcAddress getDhcAddress() {
		return dhcAddress;
	}

}
