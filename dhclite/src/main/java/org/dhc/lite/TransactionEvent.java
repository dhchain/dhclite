package org.dhc.lite;

import org.dhc.blockchain.Transaction;
import org.dhc.util.Event;

public class TransactionEvent implements Event {

	private Transaction transaction;

	public TransactionEvent(Transaction transaction) {
		this.transaction = transaction;
	}

	public Transaction getTransaction() {
		return transaction;
	}

}
