package org.dhc.lite;

import org.dhc.network.Peer;
import org.dhc.util.Coin;
import org.dhc.util.DhcAddress;
import org.dhc.util.Message;

public class GetBalanceRequest extends Message {

	private DhcAddress address;

	public GetBalanceRequest(DhcAddress address) {
		this.address = address;
	}

	@Override
	public void process(Peer peer) {
		Coin balance = null;
		Message message  = new GetBalanceReply(balance);
		message.setCorrelationId(getCorrelationId());
		peer.send(message);
	}

	public DhcAddress getAddress() {
		return address;
	}

}
