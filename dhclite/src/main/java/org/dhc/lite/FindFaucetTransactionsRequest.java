package org.dhc.lite;

import java.util.Set;

import org.dhc.blockchain.Transaction;
import org.dhc.network.Peer;
import org.dhc.util.DhcAddress;
import org.dhc.util.Message;

public class FindFaucetTransactionsRequest extends Message {

	private DhcAddress address;
	private String ip;

	public FindFaucetTransactionsRequest(DhcAddress address, String ip) {
		this.address = address;
		this.ip = ip;
	}

	@Override
	public void process(Peer peer) {
		Set<Transaction> transactions = null;
		Message message  = new GetTransactionsReply(transactions);
		message.setCorrelationId(getCorrelationId());
		peer.send(message);

	}

	public DhcAddress getAddress() {
		return address;
	}

	public String getIp() {
		return ip;
	}


}
