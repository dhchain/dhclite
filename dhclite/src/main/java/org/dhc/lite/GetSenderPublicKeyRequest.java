package org.dhc.lite;

import org.dhc.network.Peer;
import org.dhc.util.Message;

public class GetSenderPublicKeyRequest extends Message {

	private String address;

	public GetSenderPublicKeyRequest(String address) {
		this.address = address;
	}

	@Override
	public void process(Peer peer) {

	}

	public String getAddress() {
		return address;
	}


}
