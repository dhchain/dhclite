package org.dhc.lite;

import org.dhc.blockchain.Transaction;
import org.dhc.gui.promote.JoinTransactionEvent;
import org.dhc.network.Peer;
import org.dhc.util.DhcAddress;
import org.dhc.util.DhcLogger;
import org.dhc.util.Listeners;
import org.dhc.util.Message;

public class NotifyTransactionMessage extends Message {
	
	private static final DhcLogger logger = DhcLogger.getLogger();

	private Transaction transaction;

	public NotifyTransactionMessage(Transaction transaction) {
		this.transaction = transaction;
	}

	@Override
	public void process(Peer peer) {
		if (alreadySent(toString())) {
			return;
		}
		
		DhcAddress myDhcaddress = DhcAddress.getMyDhcAddress();
		
		if(!transaction.getSenderDhcAddress().equals(myDhcaddress) && !transaction.getReceiver().equals(myDhcaddress)) {
			return;
		}
		
		logger.info("Saved transaction {}", transaction);
		Listeners.getInstance().sendEvent(new JoinTransactionEvent(transaction));
		Listeners.getInstance().sendEvent(new TransactionEvent(transaction));
	}

	public Transaction getTransaction() {
		return transaction;
	}
	
	@Override
	public String toString() {
		String str = String.format("NotifyTransactionMessage %s", transaction.getTransactionId());
		return str;
	}

}
