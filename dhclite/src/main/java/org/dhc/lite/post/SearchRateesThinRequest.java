package org.dhc.lite.post;

import java.util.Set;

import org.dhc.network.Peer;
import org.dhc.util.Message;

public class SearchRateesThinRequest extends Message {

	private String account;
	private Set<String> words;

	public SearchRateesThinRequest(String account, Set<String> words) {
		this.account = account;
		this.words = words;
	}

	@Override
	public void process(Peer peer) {

	}


	public Set<String> getWords() {
		return words;
	}

	public String getAccount() {
		return account;
	}

}
