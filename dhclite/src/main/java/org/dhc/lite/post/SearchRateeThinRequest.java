package org.dhc.lite.post;

import org.dhc.network.Peer;
import org.dhc.util.DhcAddress;
import org.dhc.util.Message;

public class SearchRateeThinRequest extends Message {
	
	private DhcAddress dhcAddress;
	private String transactionId;

	public SearchRateeThinRequest(DhcAddress dhcAddress, String transactionId) {
		super();
		this.dhcAddress = dhcAddress;
		this.transactionId = transactionId;
	}

	@Override
	public void process(Peer peer) {
		

	}

	public String getTransactionId() {
		return transactionId;
	}

	public DhcAddress getDhcAddress() {
		return dhcAddress;
	}

}
