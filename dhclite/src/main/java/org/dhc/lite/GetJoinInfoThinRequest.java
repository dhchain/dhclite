package org.dhc.lite;

import org.dhc.gui.promote.JoinInfo;
import org.dhc.network.Peer;
import org.dhc.util.Coin;
import org.dhc.util.DhcAddress;
import org.dhc.util.Message;

public class GetJoinInfoThinRequest extends Message {

	private DhcAddress myDhcAddress;
	private DhcAddress address;
	private Coin amount;

	public GetJoinInfoThinRequest(DhcAddress myDhcAddress, DhcAddress address, Coin amount) {
		this.myDhcAddress = myDhcAddress;
		this.address = address;
		this.amount = amount;
		
	}

	@Override
	public void process(Peer peer) {
		JoinInfo joinInfo = null;
		Message message  = new GetJoinInfoThinReply(joinInfo);
		message.setCorrelationId(getCorrelationId());
		peer.send(message);
	}

	public DhcAddress getMyDhcAddress() {
		return myDhcAddress;
	}

	public DhcAddress getAddress() {
		return address;
	}

	public Coin getAmount() {
		return amount;
	}


}
