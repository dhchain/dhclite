package org.dhc.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;

public class Configurator {

	private static final DhcLogger logger = DhcLogger.getLogger();
	
	private static final String APP_PROPERTIES = Constants.FILES_DIRECTORY + "config/dhc.properties";
	private static final Configurator instance = new Configurator();

	private Properties props = null;

	private Configurator() {
		props = new Properties();
		try {
			logger.info("Constants.FILES_DIRECTORY={}", Constants.FILES_DIRECTORY);
			logger.info("APP_PROPERTIES={}", APP_PROPERTIES);
			File file =  new File(APP_PROPERTIES);
			if (!file.exists()) {
				try (OutputStream os = new FileOutputStream(file)) {
					os.write("\n".getBytes());
					os.close();
				}
			}
			props.load(new FileInputStream(APP_PROPERTIES));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}

	public static Configurator getInstance() {
		return instance;
	}

	public String getProperty(String key) {
		return (String) props.get(key);
	}
	
	public int getIntProperty(String key) {
		String str = getProperty(key);
		if(str == null) {
			return 0;
		}
		try {
			return Integer.parseInt(str);
		} catch (Exception e) {
			return 0;
		}
	}
	
	public int getIntProperty(String key, int defaultValue) {
		int result = getIntProperty(key);
		result = result == 0 ? defaultValue : result;
		return result;
	}
	
	public boolean getBooleanProperty(String key) {
		String str = getProperty(key);
		if("true".equals(str)) {
			return true;
		}
		return false;
	}

}