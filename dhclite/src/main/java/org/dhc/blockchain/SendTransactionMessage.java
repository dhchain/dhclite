package org.dhc.blockchain;

import org.dhc.network.Network;
import org.dhc.network.Peer;
import org.dhc.util.DhcLogger;
import org.dhc.util.Message;

public class SendTransactionMessage extends Message {
	
	private static final DhcLogger logger = DhcLogger.getLogger();
	
	private Transaction transaction;

	public SendTransactionMessage(Transaction transaction) {
		this.transaction = transaction;
	}

	@Override
	public void process(Peer peer) {

	}
	
	@Override
	public String toString() {
		String str = String.format("SendTransactionMessage-%s", transaction.getTransactionId());
		return str;
	}
	
	@Override
	public void failedToSend(Peer peer, Exception e) {
		logger.trace("Failed to send  {}", this);
		logger.trace("Failed to send to peer {}", peer);
		Network.getInstance().sendToAllMyPeers(this);
	}

}
